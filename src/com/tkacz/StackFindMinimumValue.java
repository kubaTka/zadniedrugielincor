package com.tkacz;

import java.util.Stack;

public class StackFindMinimumValue {


    public final Stack<Integer> stack = new Stack<>();
    public final Stack<Integer> stackMin = new Stack<>();


    public void push(int value) {
        stack.push(value);
        if (stackMin.isEmpty()) {
            stackMin.push(value);
        } else if (value < stackMin.peek()) {
            stackMin.push(value);

        }
    }

    public int pop() {
        int value = stack.pop();
        if (stack.isEmpty()) {
            throw new IllegalStateException("stack is empty");
        } else if (value == stackMin.peek()) {
            stackMin.pop();
        }
        return value;
    }
    public int min(){
        if (stack.isEmpty()) {
            throw new IllegalStateException("stack is empty");
        } return stackMin.peek();

    }

}



