package com.tkacz;

public class ClassFindMinimumValue {
    public static void main(String[] args) {

        StackFindMinimumValue findMin = new StackFindMinimumValue();
        findMin.push(2);
        findMin.push(1);
        findMin.push(3);
        findMin.push(5);
        findMin.pop();
        System.out.println(findMin.min());
    }
}
